'use strict';

angular.module('myApp')

  .config(['$routeProvider', function ($routeProvider) {
    $routeProvider.when('/booking', {
      templateUrl: 'booking/booking.html',
      controller: 'BookingCtrl as ctrl',
      bindToController: true
    });
  }])

  .controller('BookingCtrl', ['BookingService', '$filter', '$location', function (BookingService, $filter, $location) {
    var ctrl = this;

    ctrl.treatments = [];
    ctrl.packages = [];
    ctrl.times = [];
    ctrl.errors = [];
    ctrl.selectedTreatment = '';
    ctrl.selectedTreatmentName = '';
    ctrl.selectedPackage = '';
    ctrl.selectedPackageName = '';
    ctrl.selectedTime = '';
    ctrl.selectedTimeName = '';
    ctrl.selectedDate = null;

    var init = function () {

      BookingService.getTreatments()
        .then(function (resp) {
          ctrl.treatments = resp.data;
        })
        .catch(function () {
          ctrl.errors.push('Unable to get treatments');
        });
    };

    ctrl.treatmentSelected = function (treatment) {
      ctrl.selectedTreatment = treatment;

      // now get packages for selected treatment
      BookingService.getPackages(treatment.id)
        .then(function (resp) {
          ctrl.packages = resp.data;
        })
        .catch(function (err) {
          ctrl.errors.push('Unable to get prices');
        });
    };

    ctrl.packageSelected = function (selPackage) {
      ctrl.selectedPackage = selPackage;
      ctrl.selectedPackageName = '£' + selPackage.price;

      // get the times
      BookingService.getTimes()
        .then(function (resp) {
          ctrl.times = resp.data;
        })
        .catch(function () {
          ctrl.errors.push('Unable to get times');
        });
    };

    ctrl.timeSelected = function (selTime) {
      ctrl.selectedTime = selTime;
      ctrl.selectedTimeName = $filter('date')(ctrl.selectedDate, 'dd mm yyyy') + ' - ' + selTime.timeOfDay;
    };

    ctrl.isBookingDisabled = function () {
      return ctrl.selectedPackage === '' || ctrl.selectedTreatment === '' || ctrl.selectedTime === '';
    };

    ctrl.book = function () {
      $location.url('booked');
    };

    init();
  }]);