'use strict';

angular.module('myApp')

.config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/booked', {
    templateUrl: 'booked/booked.html',
    controller: 'BookedCtrl as ctrl'
  });
}])

.controller('BookedCtrl', ['$location', function($location) {
  var ctrl = this;

  ctrl.booking = function() {
    $location.url('booking');
  }
}]);