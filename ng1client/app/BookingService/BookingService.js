angular.module('myApp')
  .service('BookingService', ['$http', '$q', function ($http, $q) {

    var baseUrl = 'http://ec2-52-38-80-50.us-west-2.compute.amazonaws.com';

    this.getTreatments = function () {

      return $http.get(baseUrl + '/treatments');
    };

    this.getTimes = function () {
      return $http.get(baseUrl + '/times');
    };

    this.getPackages = function (treatmentId) {
      var params = {
        params: {
          treatmentId: treatmentId.toString()
        }
      };

      return $http.get(baseUrl + '/prices', params);
    };

  }]);
