## Instructions

# pre-requisites
node - at least v4  
npm  
angular-cli - to install use npm i angular-cli -g  

# server
run CreateSchema in mysql workbench to set up db  
run npm i to install all libraries  
run npm start to run server  
run npm test to run tests (server needs to be running) on linux  
to run tests on windows, install mocha using npm i mocha -g, then run mocha tests  

# client
run npm i to install all libraries  
run npm start to run app, and point browser to localhost:4200  
run npm test to run tests  