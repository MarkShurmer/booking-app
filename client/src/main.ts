import {bootstrap} from '@angular/platform-browser-dynamic';
import {disableDeprecatedForms, provideForms} from '@angular/forms';
import {enableProdMode} from '@angular/core';
import {HTTP_PROVIDERS} from '@angular/http';
import {BookingAppAppComponent, environment} from './app/';
import {appRouterProviders} from './app/booking-app.component';

if (environment.production) {
  enableProdMode();
}

bootstrap(BookingAppAppComponent, [
  disableDeprecatedForms(),
  provideForms(),
  HTTP_PROVIDERS,
  appRouterProviders
])
  .catch((err:any) => console.log(err));
