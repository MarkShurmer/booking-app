import {Injectable} from "@angular/core";
import {Http, Response, Request, RequestOptions, URLSearchParams} from "@angular/http";
import {Observable} from "rxjs/Rx";
import { environment } from '../environment';

@Injectable()
export class BookingService {
  private baseUrl =   'http://ec2-52-38-80-50.us-west-2.compute.amazonaws.com';

  constructor(private http: Http) {

  }

  getTreatments():Observable<any> {
    return this.http.get(environment.baseUrl + '/treatments')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getTimes():Observable<any> {
    return this.http.get(environment.baseUrl +'/times')
      .map(this.extractData)
      .catch(this.handleError);
  }

  getPackages(treatmentId: number):Observable<any> {
    let params: URLSearchParams = new URLSearchParams();
    params.set('treatmentId', treatmentId.toString());
    return this.http.get(environment.baseUrl +'/prices', { search: params })
      .map(this.extractData)
      .catch(this.handleError);
  }

  private extractData(res: Response) {
    let body = res.json();
    return body || { };
  }

  private handleError (error: any) {
    // In a real world app, we might use a remote logging infrastructure
    // We'd also dig deeper into the error to get a better message
    let errMsg = (error.message) ? error.message :
      error.status ? `${error.status} - ${error.statusText}` : 'Server error';
    console.error(errMsg); // log to console instead
    return Observable.throw(errMsg);
  }
}
