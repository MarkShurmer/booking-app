import { Component } from '@angular/core';
import {BookingPageComponent} from "./booking-page/booking-page.component";
import { ROUTER_DIRECTIVES } from '@angular/router';
import { provideRouter, RouterConfig } from '@angular/router';
import {BookedPageComponent} from "./booked-page/booked-page.component";

const routes: RouterConfig = [
  { path: 'booking', component: BookingPageComponent },
  { path: 'booked', component: BookedPageComponent },
  { path: '**', component: BookingPageComponent }
];

export const appRouterProviders = [
  provideRouter(routes)
];

@Component({
  moduleId: module.id,
  selector: 'booking-app',
  templateUrl: 'booking-app.component.html',
  styleUrls: ['booking-app.component.css', '../assets/materialize.min.css'],
  directives: [BookingPageComponent, ROUTER_DIRECTIVES]
})
export class BookingAppAppComponent {
  title = 'Blow Booking';

}
