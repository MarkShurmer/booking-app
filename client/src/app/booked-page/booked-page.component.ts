import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'app-booked-page',
  templateUrl: 'booked-page.component.html'
})
export class BookedPageComponent implements OnInit {

  constructor(private router: Router) {}

  ngOnInit() {
  }

  booking() {
    this.router.navigateByUrl('booking');
  }
}
