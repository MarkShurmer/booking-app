import {
  beforeEachProviders,
  describe,
  expect,
  it,
  inject
} from '@angular/core/testing';
import { BookingAppAppComponent } from '../app/booking-app.component';

beforeEachProviders(() => [BookingAppAppComponent]);

describe('App: BookingApp', () => {
  it('should create the app',
      inject([BookingAppAppComponent], (app: BookingAppAppComponent) => {
    expect(app).toBeTruthy();
  }));

  it('should have as title \'Blow Booking\'',
      inject([BookingAppAppComponent], (app: BookingAppAppComponent) => {
    expect(app.title).toEqual('Blow Booking');
  }));
});
