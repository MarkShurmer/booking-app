import {Component, OnInit} from '@angular/core';
import {NgForm}    from '@angular/forms';
import {MdInput} from "@angular2-material/input/input";
import {MdList} from "@angular2-material/list/list";
import {BookingService} from "../booking-service/booking-service";
import {MdIcon} from "@angular2-material/icon/icon";
import {MdButton} from "@angular2-material/button/button";
import {Router} from "@angular/router";

@Component({
  moduleId: module.id,
  selector: 'booking-page',
  templateUrl: 'booking-page.component.html',
  styleUrls: ['booking-page.component.css'],
  providers: [BookingService],
  directives: [MdInput, MdList, MdIcon, MdButton]
})
export class BookingPageComponent implements OnInit {
  treatments:Array<any>;
  packages:Array<any>;
  times:Array<any>;
  errors:Array<string>;
  selectedTreatment:any;
  isTreatmentVisible:boolean;
  selectedTreatmentName:string;
  selectedPackage:any;
  isPackageVisible:boolean;
  selectedPackageName:string;
  selectedTime:any;
  isTimeVisible:boolean;
  selectedTimeName:string;
  isBookingDisabled:boolean;

  constructor(private bookingService:BookingService, private router:Router) {
    this.treatments = [];
    this.packages = [];
    this.times = [];
    this.errors = new Array<string>();
  }

  ngOnInit() {
    const that = this;
    this.bookingService.getTreatments().subscribe(
      t => that.treatments = t,
      () => that.errors.push('Unable to get treatments')
    );

    this.isBookingDisabled = true;
  }

  treatmentSelected(treatment) {
    const that = this;
    this.selectedTreatment = treatment;
    this.selectedTreatmentName = treatment.name;
    this.isTreatmentVisible = false; // close up the panel
    this.checkEnablingBooking();

    this.bookingService.getPackages(treatment.id).subscribe(
      p => that.packages = p,
      err => that.errors.push('Unable to get prices'),
      () => that.openPackage()
    )
  }

  openTreatment() {
    this.isTreatmentVisible = true;
  }

  closeTreatment() {
    this.isTreatmentVisible = false;
  }

  packageSelected(selPackage) {
    const that = this;
    this.selectedPackage = selPackage;
    this.selectedPackageName = '£' + selPackage.price;
    this.isPackageVisible = false; // close up the panel
    this.checkEnablingBooking();

    // get the times
    this.bookingService.getTimes().subscribe(
      p => that.times = p,
      err => that.errors.push('Unable to get times'),
      () => that.openTime()
    )
  }

  openPackage() {
    this.isPackageVisible = true;
  }

  closePackage() {
    this.isPackageVisible = false;
  }

  timeSelected(selTime) {
    this.selectedTime = selTime;
    this.selectedTimeName = selTime.timeOfDay;
    this.isTimeVisible = false; // close up the panel
    this.checkEnablingBooking();
  }

  openTime() {
    this.isTimeVisible = true;
  }

  closeTime() {
    this.isTimeVisible = false;
  }

  checkEnablingBooking():void {
    this.isBookingDisabled = this.selectedPackage === undefined || this.selectedTreatment === undefined || this.selectedTime === undefined;
  }

  book() {
    this.router.navigateByUrl('booked');
  }
}
