import {
  beforeEach,
  beforeEachProviders,
  describe,
  expect,
  it,
  inject,
} from '@angular/core/testing';
import { ComponentFixture, TestComponentBuilder } from '@angular/compiler/testing';
import { Component } from '@angular/core';
import { By } from '@angular/platform-browser';
import { BookingPageComponent } from './booking-page.component';
import {BookingService} from "../booking-service/booking-service";

// mocks
var mockBookingService = jasmine.createSpyObj('BookingService', ['getTreatments']);

describe('Component: BookingPage', () => {
  let builder: TestComponentBuilder;

  beforeEachProviders(() => [BookingPageComponent]);
  beforeEach(inject([TestComponentBuilder], function (tcb: TestComponentBuilder) {
    builder = tcb;
  }));

  it('should create the component', inject([], () => {
    return builder.createAsync(BookingPageComponentTestController)
      .then((fixture: ComponentFixture<any>) => {
        let query = fixture.debugElement.query(By.directive(BookingPageComponent));
        expect(query).toBeTruthy();
        expect(query.componentInstance).toBeTruthy();
      });
  }));
});

@Component({
  selector: 'test',
  template: `
    <app-booking-page></app-booking-page>
  `,
  directives: [BookingPageComponent]
})
class BookingPageComponentTestController {
}

