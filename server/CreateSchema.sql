drop database IF EXISTS blow;
create database IF NOT EXISTS blow;
-- DROP USER 'blow'@'localhost';
CREATE USER 'blow'@'localhost' IDENTIFIED BY 'blow01';
GRANT ALL PRIVILEGES ON * . * TO 'blow'@'localhost';
use blow;

create table treatments (
    id int(11) NOT NULL AUTO_INCREMENT,
    name varchar(200) not null,
    PRIMARY KEY (id)
) ENGINE=InnoDB;

CREATE TABLE prices (
    id INT(11) NOT NULL AUTO_INCREMENT,
    treatmentId INT(11) NOT NULL,
    duration INT(11) NOT NULL,
    price DECIMAL(10 , 2 ) NOT NULL,
    FOREIGN KEY (treatmentId)
        REFERENCES treatments (id)
        ON UPDATE CASCADE,
    PRIMARY KEY (id)
)  ENGINE=INNODB;

create table times (
    id int(11) NOT NULL AUTO_INCREMENT,
    timeOfDay nchar(5) not null,

    PRIMARY KEY (id)
) ENGINE=InnoDB;


INSERT INTO treatments VALUES (1, 'Blow dry');
INSERT INTO treatments VALUES (2, 'Make up');
INSERT INTO treatments VALUES (3, 'Nails');

INSERT INTO prices VALUES (1, 1, 40, 60);
INSERT INTO prices VALUES (2, 1, 60, 80);
INSERT INTO prices VALUES (3, 2, 30, 90);
INSERT INTO prices VALUES (4, 2, 50, 120);
INSERT INTO prices VALUES (5, 3, 50, 80);
INSERT INTO prices VALUES (6, 3, 70, 90);
INSERT INTO prices VALUES (7, 3, 90, 100);

INSERT INTO times VALUES (1, '08:00');
INSERT INTO times VALUES (2, '08:30');
INSERT INTO times VALUES (3, '09:00');
INSERT INTO times VALUES (4, '09:30');
INSERT INTO times VALUES (5, '10:00');
INSERT INTO times VALUES (6, '10:30');
INSERT INTO times VALUES (7, '11:00');
INSERT INTO times VALUES (8, '11:30');