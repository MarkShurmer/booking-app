var should = require('should');
var assert = require('assert');
var request = require('supertest');
var common = require('../routes/common.js');


describe('Tests for prices route', function () {
  var url = 'http://localhost:8000';

  describe('get', function () {
    it('should get error with no treatmentid', function (done) {

      // send request to server
      request(url)
        .get('/prices')
        .expect(422)
        // end handles the response
        .end(function (err, res) {
          console.log();
          res.text.should.be.equal('TreatmentId was needed');

          done();
        });
    });

    it('should get list of prices', function (done) {

      // send request to server
      request(url)
        .get('/prices?treatmentId=1')
        .expect(200)
        // end handles the response
        .end(function (err, res) {
          should.not.exist(err);
          console.log(res.body);

          res.body.should.not.equal(null);
          res.body.length.should.be.above(1);
          done();
        });
    });
  });

});
