var should = require('should');
var assert = require('assert');
var request = require('supertest');
var common = require('../routes/common.js');


describe('Tests for times route', function () {
  var url = 'http://localhost:8000';
  var connection;

  describe('get', function () {
    it('should get list of treatments', function (done) {

      // send request to server
      request(url)
        .get('/treatments')
        .expect(200)
        // end handles the response
        .end(function (err, res) {
          should.not.exist(err);
          // this is should.js syntax
          res.body.should.not.equal(null);
          res.body.length.should.be.above(1);
          done();
        });
    });
  });

});
