var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');
var config = require('./env.json')[process.env.NODE_ENV || 'development'];

var treatments = require('./routes/treatments');
var times = require('./routes/times');
var prices = require('./routes/prices');

var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

// Add headers
app.use(function (req, res, next) {

  console.log('adding headers');

  // Website you wish to allow to connect
  res.setHeader('Access-Control-Allow-Origin', '*');

  // Request methods you wish to allow
  res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

  // Request headers you wish to allow
  res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type,Auth,UserInfo,X-CSRF-Token');

  // Set to true if you need the website to include cookies in the requests sent
  // to the API (e.g. in case you use sessions)
  res.setHeader('Access-Control-Allow-Credentials', true);

  // Check if preflight request
  if (req.method === 'OPTIONS') {
    res.status(200);
    res.end();
  }
  else {
    // Pass to next layer of middleware
    next();
  }
});

app.use(express.static('../client'));

treatments.treatments(app);
times.times(app);
prices.prices(app);

module.exports = app;

console.log('listening on port ' + config.port);
app.listen(config.port);