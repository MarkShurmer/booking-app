var exports = module.exports = {};
var mysql = require('promise-mysql');
var q = require('q');
var config = require('../env.json')[process.env.NODE_ENV || 'development'];

function connectToDb() {
    var deferred = q.defer('conn');
  console.log(config.MYSQL_USER.name);

    mysql.createConnection({
        host: config.MYSQL_SERVER.host,
        user: config.MYSQL_USER.name,
        password: config.MYSQL_USER.password,
        database: config.MYSQL_SERVER.database
        })
        .then(function (conn) {
            // if (logger) {
            //     logger.info(conn);
            // }
            deferred.resolve(conn);
        })
        .catch(function (err) {
          console.log(err);
            deferred.reject(err);

        });

    return deferred.promise;
}


exports.connectToDb = connectToDb;
