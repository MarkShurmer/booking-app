var common = require('./common.js');
var exports = module.exports = {};

var prices = function (app) {
  app.get('/prices', function (req, res) {

    if (!req.query.hasOwnProperty('treatmentId')) {
      res.status(422);
      res.send('TreatmentId was needed');
    } else {

      common.connectToDb()
        .then(function (conn) {
          conn.query('select id, duration, price from prices where treatmentId = ?', [req.query.treatmentId])
            .then(function (data) {
              res.send(data);
            })
        })
        .catch(function (err) {
          res.status(500);
          res.send(err);
        });
    }
  });
};

exports.prices = prices;

