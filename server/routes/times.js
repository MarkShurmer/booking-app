var common = require('./common.js');
var exports = module.exports = {};

var times = function (app) {
  app.get('/times', function (req, res) {
    common.connectToDb()
      .then(function (conn) {
        conn.query('select * from times')
          .then(function (data) {
            res.send(data);
          })
      })
      .catch(function (err) {
        res.status(501);
        res.send(err);
      });
  });
};

exports.times = times;

