var common = require('./common.js');
var exports = module.exports = {};

var treatments = function (app) {
    app.get('/treatments', function (req, res) {
        common.connectToDb()
            .then(function (conn) {
                conn.query('select * from treatments')
                    .then(function (data) {
                        console.log(data);
                        res.send(data);
                    })
            })
            .catch(function (err) {
                res.status(500);
                res.send(err);
            });
    });
};

exports.treatments = treatments;

